#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Board *playBoard;
    Side oppSide;
    Side selfSide;
    bool canMove;
    vector<Move*> moveList(Board *board, Side side);
    Move *doMove(Move *opponentsMove, int msLeft);
	int getScore(Board *board, Side side);
	int alphabeta(Board *currentBoard, int depth, int alpha, int beta, bool self);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
