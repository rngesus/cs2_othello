#include "player.h"
#include <cstdlib>
#include <cmath>
#include "stdio.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    playBoard = new Board();
	selfSide = side;
	canMove = playBoard->hasMoves(selfSide);
    oppSide = BLACK;
    if(side == BLACK){
		oppSide = WHITE;
	}
    

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {

}
int Player::getScore(Board *board, Side side) {
	int blackScore = 0;
	int whiteScore = 0;
	blackScore += board->countWhite();
	whiteScore += board->countBlack();
	
	if(board->get(BLACK, 0, 0)) {
		blackScore += 200;
	}
	if(board->get(BLACK, 0, 7)) {
		blackScore += 200;
	}
	if(board->get(BLACK, 7, 0)) {
		blackScore += 200;
	}
	if(board->get(BLACK, 7, 7)) {
		blackScore += 200;
	}
	
	if(board->get(WHITE, 0, 0)) {
		whiteScore += 200;
	}
	if(board->get(WHITE, 0, 7)) {
		whiteScore += 200;
	}
	if(board->get(WHITE, 7, 0)) {
		whiteScore += 200;
	}
	if(board->get(WHITE, 7, 7)) {
		whiteScore += 200;
	}
	
	if(board->get(BLACK, 0, 1)) {
		blackScore -= 50;
	}
	if(board->get(BLACK, 1, 0)) {
		blackScore -= 50;
	}
	if(board->get(BLACK, 1, 1)) {
		blackScore -= 100;
	}
	
	if(board->get(BLACK, 0, 6)) {
		blackScore -= 50;
	}
	if(board->get(BLACK, 1, 7)) {
		blackScore -= 50;
	}
	if(board->get(BLACK, 1, 6)) {
		blackScore -= 100;
	}
	
	if(board->get(BLACK, 7, 1)) {
		blackScore -= 50;
	}
	if(board->get(BLACK, 6, 0)) {
		blackScore -= 50;
	}
	if(board->get(BLACK, 6, 1)) {
		blackScore -= 100;
	}
	
	if(board->get(BLACK, 7, 6)) {
		blackScore -= 50;
	}
	if(board->get(BLACK, 6, 7)) {
		blackScore -= 50;
	}
	if(board->get(BLACK, 6, 6)) {
		blackScore -= 100;
	}
	
	if(board->get(WHITE, 0, 1)) {
		whiteScore -= 50;
	}
	if(board->get(WHITE, 1, 0)) {
		whiteScore -= 50;
	}
	if(board->get(WHITE, 1, 1)) {
		whiteScore -= 100;
	}
	
	if(board->get(WHITE, 0, 6)) {
		whiteScore -= 50;
	}
	if(board->get(WHITE, 1, 7)) {
		whiteScore -= 50;
	}
	if(board->get(WHITE, 1, 6)) {
		whiteScore -= 100;
	}
	
	if(board->get(WHITE, 7, 1)) {
		whiteScore -= 50;
	}
	if(board->get(WHITE, 6, 0)) {
		whiteScore -= 50;
	}
	if(board->get(WHITE, 6, 1)) {
		whiteScore -= 100;
	}
	
	if(board->get(WHITE, 7, 6)) {
		whiteScore -= 50;
	}
	if(board->get(WHITE, 6, 7)) {
		whiteScore -= 50;
	}
	if(board->get(WHITE, 6, 6)) {
		whiteScore -= 100;
	}
		
	for(int i = 2; i < 6; i++) {
		if(board->get(WHITE, i, 0)) {
			whiteScore += 20;
		}
		if(board->get(BLACK, i, 0)) {
			blackScore += 20;
		}
		if(board->get(WHITE, i, 7)) {
			whiteScore += 20;
		}
		if(board->get(BLACK, i, 7)) {
			blackScore += 20;
		}
		if(board->get(WHITE, 0, i)) {
			whiteScore += 20;
		}
		if(board->get(BLACK, 0, i)) {
			blackScore += 20;
		}
		if(board->get(WHITE, 7, i)) {
			whiteScore += 20;
		}
		if(board->get(BLACK, 7, i)) {
			blackScore += 20;
		}
	}
	
	if(side == BLACK) {
		return blackScore - whiteScore;
	}
	if(side == WHITE) {
		return whiteScore - blackScore;
	}
}

vector<Move*> Player::moveList(Board *board, Side side){
	vector<Move*> list;
	for(int row = 0; row < 8; row++){
		for(int col = 0; col < 8; col++){
			Move *testMove = new Move(row, col);
			if(board->checkMove(testMove, side)){
				list.push_back(testMove);
			}	
		}
	}
	return list;
}

int Player::alphabeta(Board *currentBoard, int depth, int alpha, int beta, bool self){
	
	if(depth == 0){
		if(self){
			return getScore(currentBoard, selfSide);
		}
		else{
			return getScore(currentBoard, oppSide);
		}
	}
	
	if(self){
		Board *testBoard = currentBoard->copy();
		vector<Move*> possible = moveList(testBoard, selfSide);
		
		if(possible.size() == 0){
			return getScore(currentBoard, selfSide);
		}
		
		for(int i = 0; i < possible.size();  i++){
			testBoard = currentBoard->copy();
			testBoard->doMove(possible[i], selfSide);
			alpha = max(alpha, alphabeta(testBoard, depth -1, alpha, beta, false));
			if(beta <= alpha){
				break;
			}
			
		}
		return alpha;
	}
	else{
		Board *testBoard = currentBoard->copy();
		vector<Move*> possible = moveList(testBoard, oppSide);
		
		if(possible.size() == 0){
			return getScore(currentBoard, selfSide);
		}
		
		for(int i = 0; i < possible.size();  i++){
			testBoard = currentBoard->copy();
			testBoard->doMove(possible[i], oppSide);
			beta = min(beta, alphabeta(testBoard, depth -1, alpha, beta, true));
			if(beta <= alpha){
				break;
			}
			
		}
		return beta;
	}
	
	
	
}
 

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
 
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    
    if(opponentsMove != NULL){
		playBoard->doMove(opponentsMove, oppSide);
	}
	
	if(!playBoard->hasMoves(selfSide) || msLeft == -1){
		return NULL;
	}
	
	int max = -10000;
	Board *testBoard = playBoard->copy();
	vector<Move*> possible = moveList(testBoard, selfSide);
	Move* bestMove = NULL;
	for(int i = 0; i < possible.size(); i++){
		testBoard = playBoard->copy();
		testBoard->doMove(possible[i], selfSide);
		int alpha = alphabeta(testBoard, 7, -10000, 10000, false);
		if(alpha > max){
			bestMove = possible[i];
			max = alpha;
		}
	}
	
	if(bestMove != NULL){
		playBoard->doMove(bestMove, selfSide);
		return bestMove;
	}

	
	
	 /* 
      * RANDOM PLAYER
      
	std::vector<Move*> possible = moveList(playBoard, selfSide);
	if(playBoard->hasMoves(selfSide)){
		int rand_spot = rand() % possible.size();
		playBoard->doMove(possible[rand_spot], selfSide);
		return possible[rand_spot];
	}
    
    */
    
    if(testingMinimax){
		int selfScore, oppScore;
	
		double bestChange = -500;

		Board *testBoard = playBoard->copy();
		vector<Move*> possible = moveList(testBoard, selfSide);
		Move* bestMove = possible[0];
		
		for(int i = 0; i < possible.size(); i++){
			
			testBoard = playBoard->copy();
			testBoard->doMove(possible[i], selfSide);
			vector<Move*> move2 = moveList(testBoard, oppSide);
			Board *testBoard2 = testBoard->copy();
			
			int minScore = 100;
			
			for(int j = 0; j < move2.size(); j++){
				double change;
				testBoard2 = testBoard->copy();
				testBoard2->doMove(move2[j], oppSide);
				
				if(selfSide == WHITE){
					change = testBoard2->countWhite() - testBoard2->countBlack();
				}
				else{
					change = testBoard2->countBlack() - testBoard2->countWhite();
				}
				
				if(change < minScore){
					minScore = change;
				}

				
			}

			if(minScore >= bestChange){
				bestChange = minScore;
				bestMove = possible[i];
			}
			
		}
		playBoard->doMove(bestMove, selfSide);
		return bestMove;	
	}
    
    //HEURISTIC 
  /*  int best_movescore = 0;
    int best_movespot = 0;
    std::vector<Move*> possible = moveList(playBoard, selfSide);
    std::vector<Move*> best_moves;
    for(int i = 0; i < possible.size(); i++){
		int point_count = 0;
		Board *testBoard = playBoard->copy();
		if(possible[i]->getX() == 0 || possible[i]->getX() == 7){
			if(possible[i]->getY() == 0 || possible[i]->getY() == 7){
				point_count += 10;
			}
			else if(possible[i]->getY() == 1 || possible[i]->getY() == 6){
				point_count -= 5;
			}
			else{
				point_count += 5;
			}
		}
		else if(possible[i]->getX() == 1 || possible[i]->getX() == 6){
			if(possible[i]->getY() == 0 || possible[i]->getY() == 7){
				point_count -= 5;
			}
			else if(possible[i]->getY() == 1 || possible[i]->getY() == 6){
				point_count -= 10;
			}
		}
		else{
			if(possible[i]->getY() == 0 || possible[i]->getY() == 6){
				point_count += 5;
			}
		}
		testBoard->doMove(possible[i], selfSide);
		if(selfSide == BLACK){
			point_count += testBoard->countBlack();
		}
		if(selfSide == WHITE){
			point_count += testBoard->countWhite();
		}
		if(point_count > best_movescore){
				best_movescore = point_count;
				best_movespot = i;
				best_moves.clear();
				best_moves.push_back(possible[i]);
		}
		else if(point_count == best_movescore){
			best_moves.push_back(possible[i]);
		}
	}
	if(playBoard->hasMoves(selfSide)){
		if(best_moves.size() == 1){
			playBoard->doMove(possible[best_movespot], selfSide);
			return possible[best_movespot];
		}
		else{
			int rand_spot = rand() % best_moves.size();
			playBoard->doMove(possible[rand_spot], selfSide);
			return possible[rand_spot];
		}	
	}
	
	*/
	
	
		
    return NULL;
}


