We added weighted tiles to the board. Corner tiles are worth the most. Tiles adjacent to corner tiles are weighted negatively. All other edge tiles are also given extra points. We then created a recursive alpha-beta pruning method to select the optimal move to make. Our recursive alpha-beta pruning has an adjustable depth search. We tried to implement an additional check for time left to determine the depth that we checked, but were not able to.

Richard Hu created the basic heuristic AI for last week's assignment.
Patrick Hsu created the minimax for last week's assignment.
We both worked on the alpha-beta pruning and othello strategies for this week's assignment.